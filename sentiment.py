
import re
from sensitive_data import dataset,feature_set,no_of_items


def calc_prob(word,category):

	if word not in feature_set or word not in dataset[category]:
		return 0

	return float(dataset[category][word])/no_of_items[category]



